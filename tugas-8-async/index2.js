var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
readBooksPromise(10000, books[0])
.then((timeleft) => readBooksPromise(timeleft, books[1]))
.then((timeleft) => readBooksPromise(timeleft, books[2]))
.catch((timeleft) => console.log('insufficient time: '+timeleft))

