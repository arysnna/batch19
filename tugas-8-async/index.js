// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let i=0; let time=10000
readBooks(time, books[i], callback = (timeleft) => { 
    i++
    if(i<books.length){
        if(timeleft>books[i].timeSpent) {
            readBooks(timeleft, books[i], callback)
        }
    }
 });