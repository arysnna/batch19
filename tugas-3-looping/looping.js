// ==== looping while
console.log('// =============================== nomor 1');
var ilovecode = "i love coding";
var iwillbecome = "i will become mobile developer";
var key = 2;
console.log('LOOPING PERTAMA');
while (key < 21) {
  console.log(key + " - " +ilovecode);
  key += 2;
}
key = 20;
console.log('LOOPING KEDUA');
while (key > 1) {
  console.log(key + " - " +iwillbecome);
  key -= 2;
}

// ==== looping for
console.log('');
console.log('// =============================== nomor 2');
for (var x = 1; x<21 ; x++) {
  if (x%3==0 && x%2 != 0) {
    console.log( x +" - I Love Coding");
  }else if (x%2==0) {
    console.log( x +" - Berkualitas");
  }else console.log( x +" - Santai");
}

// ==== Membuat Persegi Panjang
console.log('');
console.log('// =============================== nomor 3');
var k="";
for (var l=1;l<=4;l++) {
  for (var m=1;m<=8;m++) {
    k += "#";
  };
  k += "\n";
}
console.log(k);

// ==== Membuat Tangga
console.log('');
console.log('// =============================== nomor 4');
var n="";
for (var o=1;o<=7;o++) {
  for (var p=1;p<=o;p++) {
    n += "#";
  };
  n += "\n";
}
console.log(n);

// ==== Membuat Papan Catur
console.log('');
console.log('// =============================== nomor 5');
var q="";
for (var r=1;r<=8;r++) {
  if (r%2==0) {
    q += "# # # # "
  } else {
    q += " # # # #"
  }
  q += "\n";
}
console.log(q);
