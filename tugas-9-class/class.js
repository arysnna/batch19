console.log('');
console.log('// =============================== NOMOR 1');

class Animal {
    constructor(x, y = 4, z = false) { this.x=x; this.y=y; this.z=z;}
    get name() { return this.x }
    get legs() { return this.y }
    get cold_blooded() { return this.z }
    
}

class Ape extends Animal {
    constructor(x, y = 2, z = false) {
        super(x, z)
        this.y = y
    }
    yell = () => console.log('Auooo')
}

class Frog extends Animal {
    constructor(x, y = 4, z = true) {
        super(x, y)
        this.z = z
    }
    jump = () => console.log('hop hop')
}

var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('');
console.log('// =============================== NOMOR 2');

class Clock {
    constructor(template) {
        this.template = template.template
    }

    render = () => {
        let date= new Date()
        let hours= date.getHours()
        if (hours < 10) hours = '0' + hours;
        let mins= date.getMinutes()
        if (mins < 10) mins = '0' + mins;
        let secs= date.getSeconds()
        if (secs < 10) secs = '0' + secs;

        let output = this.template.toString()
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(output.toString())
    }

    start = () => {
        this.render()
        let timer = setInterval(this.render, 1000)
    }
    stop = () => clearInterval(timer)
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  