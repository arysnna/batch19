// ==== conditional if-else
console.log('// ============ conditional if-else');

var nama = "John"
var peran = "Werewolf"
var namalc = nama.toLowerCase();
var peranlc = peran.toLowerCase();

if (nama != "" && peran != "") {
  console.log("Selamat datang di Dunia Werewolf, "+nama);
  if (peranlc == 'werewolf') {
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");
  } else if (peranlc == 'penyihir') {
    console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (peranlc == 'guard') {
    console.log("Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf!");
  } else {
    console.log("Silahkan isi peran dengan Werewolf / Guard / Penyihir")
  }
} else {
  console.log('nama dan peran harus diisi!');
}

// ==== conditional switch-case
console.log('');
console.log('// ============ conditional switch-case');

var tanggal = 31; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 12; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2021; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var bulanstr;
if (tanggal>=1 && tanggal<=31 && bulan>=1 && bulan<=12 && tahun>=1900 && tahun<=2200) {
  switch (bulan) {
    case 1:
      bulanstr = 'Januari';
      break;
    case 2:
      bulanstr = 'Februari';
      break;
    case 3:
      bulanstr = 'Maret';
      break;
    case 4:
      bulanstr = 'April';
      break;
    case 5:
      bulanstr = 'Mei';
      break;
    case 6:
      bulanstr = 'Juni';
      break;
    case 7:
      bulanstr = 'Juli';
      break;
    case 8:
      bulanstr = 'Agustus';
      break;
    case 9:
      bulanstr = 'September';
      break;
    case 10:
      bulanstr = 'Oktober';
      break;
    case 11:
      bulanstr = 'November';
      break;
    case 12:
      bulanstr = 'Desember';
      break;
  }
  console.log(tanggal+ ' ' +bulanstr+ ' ' +tahun);
} else {
  console.log('Format Error, tanggal = 1 - 31 ; bulan = 1 - 12 ; tahun = 1900 - 2200');
}
