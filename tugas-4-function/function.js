// ==== nomor 1
console.log('');
console.log('// =============================== nomor 1');

function teriak() {
  return 'Halo Sanbers!';
}

console.log(teriak());

// ==== nomor 2
console.log('');
console.log('// =============================== nomor 2');

function kalikan(num1, num2) {
  return num1*num2;
}

var num1 = 12;
var num2 = 4;

var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// ==== nomor 3
console.log('');
console.log('// =============================== nomor 3');

function introduce(nama,umur,alamat,hobi) {
  return "Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!";
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
