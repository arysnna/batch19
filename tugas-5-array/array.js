// ==== nomor 1
console.log('');
console.log('// =============================== nomor 1');


function range(startNum,finishNum) {
  let x = [];
  if(startNum == null || finishNum == null) {
    return -1;
  }else{
    if(startNum>finishNum) {
      for(let i=startNum;i>=finishNum;i--) {
        x.push(i);
      }
    }else if(startNum<finishNum) {
      for(let i=startNum;i<=finishNum;i++) {
        x.push(i);
      }
    }else {
      return 'equal';
    }
    return x;
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1
console.log(range(1, 1)) // equal

// ==== nomor 2
console.log('');
console.log('// =============================== nomor 2');

function rangeWithStep(startNum, finishNum, step) {
  let x = [];
  if(startNum == null || finishNum == null) {
    return -1;
  }else{
    if(startNum>finishNum) {
      for(let i=startNum;i>=finishNum;i-=step) {
        x.push(i);
      }
    }else if(startNum<finishNum) {
      for(let i=startNum;i<=finishNum;i+=step) {
        x.push(i);
      }
    }else {
      return 'equal';
    }
    return x;
  }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

// ==== nomor 3
console.log('');
console.log('// =============================== nomor 3');

function hitung(startNum, finishNum, step){
  let x = 0;
  if(startNum>finishNum) {
    for(let i=startNum;i>=finishNum;i-=step) {
      x+=i;
    }
    return x;
  }else if(startNum<finishNum) {
    for(let i=startNum;i<=finishNum;i+=step) {
      x+=i;
    }
    return x;
  }else {
    return 'equal';
  }
}

function sum(startNum, finishNum, step) {
  let x = 0;
  if(startNum == null) {
    return 0;
  }else if(finishNum == null) {
    return startNum;
  }else{
    if (step == null) {
      step = 1;
      x = hitung(startNum, finishNum, step);
    }else{
      x = hitung(startNum, finishNum, step);
    }
    return x;
  }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// ==== nomor 4
console.log('');
console.log('// =============================== nomor 4');

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

function dataHandling(input) {
  let output = '';
  for(let x=0;x<input.length;x++) {
    var input2 = input[x];
    for(let y=0;y<input2.length;y++) {
      if(y==0){
        output += "Nomor ID:  "+ input2[y]+"\n";
      }else if(y==1){
        output += "Nama Lengkap:  "+ input2[y]+"\n";
      }else if(y==2){
        output += "TTL:  "+ input2[y]+" ";
      }else if(y==3){
        output += input2[y]+"\n";
      }else if(y==4){
        output += "Hobi:  "+ input2[y]+"\n";
      }else{
        output += "error";
      }
    }
    output += "\n";
  }
  return output;
}

console.log(dataHandling(input));

// ==== nomor 5
console.log('');
console.log('// =============================== nomor 5');

function balikKata(kata) {
  let panjang = kata.length;
  let output = '';
  for(let x=0;x<=panjang-1;x++) {
    output = output+kata[panjang-x-1];
  }
  return output;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// ==== nomor 6
console.log('');
console.log('// =============================== nomor 6');

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input) {
  let output = '';
  input.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro");
  output += input+'\n';
  console.log(input);

  let tanggal = input[3].split('/');
  let bulan = '';
  switch(tanggal[1]) {
    case '05':
      bulan='Mei';
    break;
  }
  console.log(bulan);

  let tanggaldesc = tanggal.sort(function(a,b){return b-a});
  console.log(tanggaldesc);

  let tanggaldash = input[3].split('/').join('-');
  console.log(tanggaldash);

  let nama15 = input[1].slice(0,14);
  console.log(nama15);

  return '';
}

console.log(dataHandling2(input));

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
