import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/AntDesign';

export default function App() {
    return (
        <View style={{flex: 1}}>
            <ScrollView style={styles.container}>
                <View style={styles.topNav}>
                    <TouchableOpacity style={styles.topNav}>
                        <Text style={styles.topNavTitle}>Signout</Text>
                        <Icon2 name="sign-out" size={25}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.header}>
                    <Image source={require('./assets/logo.jpg')} style={styles.headerImg} />
                </View>
                <View style={styles.profile}>
                    <Image style={styles.profileImg} source={require('./assets/avatar.jpg')} />
                    <Text style={styles.profileName} >Khairun Rizki</Text>
                </View>
                <View style={styles.profileBio}>
                    <View style={styles.profileBioBg}>
                        <Text style={{flex: 1, textAlign:'justify', flexWrap: 'wrap'}}>Tinggal di bandung, pernah bersekolah di Lorem ipsum dolor sit amet, 
                            consectetur adipiscing elit. Morbi ut justo ex. Etiam eu dui ex. Integer 
                            venenatis varius magna nec interdum. </Text>
                    </View>
                </View>
                <View style={styles.skill}>
                    <View style={styles.skillHeader}>
                            <Text style={styles.skillHeaderValue}>Links</Text>
                            <View style={{borderBottomColor:'black',borderBottomWidth: 1}}/>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon4 name="pdffile1" size={30}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>Download CV in .pdf format</Text>
                        </View>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon name="web" size={30}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>www.khairunrizki.com</Text>
                        </View>
                    </View>              
                </View>
                <View style={styles.skill}>
                    <View style={styles.skillHeader}>
                            <Text style={styles.skillHeaderValue}>Socials</Text>
                            <View style={{borderBottomColor:'black',borderBottomWidth: 1}}/>
                    </View>
                    <View style={styles.social}>
                        <View style={styles.socialList}>
                            <Icon3 name="telegram" size={50} style={{color:'#0088cc'}}/>
                            <Icon3 name="instagram" size={50} style={{color:'#DD2A7B'}}/>
                            <Icon3 name="whatsapp" size={50} style={{color:'#4FCE5D'}}/>
                            <Icon3 name="twitter" size={50} style={{color:'#00acee'}}/>
                        </View>
                    </View>                
                </View>
                <View style={{marginVertical:10}} />
            </ScrollView>
            <View style={styles.botNav}>
                <TouchableOpacity style={[styles.botNavList, styles.borderNav]}>
                    <Icon style={{color:'white', marginRight:5}} name="list" size={25}/>
                    <Text style={styles.botNavTitle}>Skill</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.botNavList, styles.activeNav]}>
                    <Icon3 style={{color:'white', marginRight:5}} name="user" size={25}/>
                    <Text style={styles.botNavTitle}>About</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 30,
    },
    topNav: {  
        height: 40,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    topNavTitle: {
        fontSize: 14,
        marginRight: 5,
    },
    header: { 
        height: 183,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
    },
    headerImg: {
        height: 183,
        width: 300,
    },
    profile: {        
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
    },
    profileImg: {
        height: 60,
        width: 60,
        borderRadius: 180,
        marginRight: 20,
    },
    profileName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#666666',
    },
    profileBio: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center', 
        marginBottom: 30,
    },
    profileBioBg: {
        width: 300, 
        borderRadius: 8,
        marginTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#F5F5F5',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    skill: {        
        marginHorizontal: 20,
        marginBottom: 20,
    },
    skillHeader: {
        marginBottom: 10,
    },
    skillHeaderValue: {
        color: '#8a8a8a',
    },
    skillList: {
        marginVertical: 10,
        display: 'flex',
        flexDirection: 'row',
    },
    skillLogo: {
        marginHorizontal: 20,
    },
    botNav: { 
        height: 60,
        display: 'flex',
        flexDirection: 'row',
        alignSelf: "stretch",
        marginTop: 10,
    },
    botNavList: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        backgroundColor: '#9c9c9c',
    },
    botNavTitle: {
        color: 'white',
        fontSize: 24,
    },
    activeNav: {
        backgroundColor: '#27A8E2',
    },
    borderNav: {
        borderRightColor: 'white',
        borderRightWidth: 3,
    },
    socialList: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    }
});