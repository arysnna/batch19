import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function App() {
    return (
        <ScrollView style={styles.container}>
            <View style={styles.header}>
                <Image source={require('./assets/logo.jpg')} style={styles.headerImg} />
            </View>
            <View style={styles.form}>
                <Text style={styles.formTitle}>Login</Text>
                <TextInput style={styles.formInput} placeholder={'Email'}/>
                <TextInput style={styles.formInput} placeholder={'Password'} secureTextEntry={true} />
                <TouchableOpacity style={styles.formButton} >
                    <Text style={styles.formButtonTitle}> login </Text>
                </TouchableOpacity>
            </View>
            <View style={styles.option}>
                <Text style={styles.optionTitle}> ──────── or ──────── </Text>
                <TouchableOpacity>
                    <Text style={styles.optionButton}> signup now </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
    },
    header: {
        height: 280,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerImg: {
        height: 183,
        width: 300,
    },
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formTitle: {
        color: '#919191',
        fontSize: 24,
        fontWeight: 'bold',
    },
    formInput: {
        height: 40, 
        width: 200, 
        borderRadius: 8,
        marginTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#F5F5F5',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    formButton: {
        height: 40, 
        width: 200, 
        borderRadius: 8,
        marginTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#0085FF',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    formButtonTitle: {
        color: 'white',
        fontSize: 18,
    },
    option: { 
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 36,
    },
    optionTitle: {
        fontSize: 14,
        fontWeight: '100', 
        color: '#9c9c9c',
    },
    optionButton: {
        color: '#6e6e6e',
        fontSize: 18,
        marginTop: 36,
    },
});