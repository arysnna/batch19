import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/Octicons';
import Icon3 from 'react-native-vector-icons/FontAwesome';
import Icon4 from 'react-native-vector-icons/FontAwesome5';
import Icon5 from 'react-native-vector-icons/MaterialCommunityIcons'

export default function App() {
    return (
        <View style={{flex: 1}}>
            <ScrollView style={styles.container}>
                <View style={styles.topNav}>
                    <TouchableOpacity style={styles.topNav}>
                        <Text style={styles.topNavTitle}>Signout</Text>
                        <Icon2 name="sign-out" size={25}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.header}>
                    <Image source={require('./assets/logo.jpg')} style={styles.headerImg} />
                </View>
                <View style={styles.profile}>
                    <Image style={styles.profileImg} source={require('./assets/avatar.jpg')} />
                    <Text style={styles.profileName} >Khairun Rizki</Text>
                </View>
                <View style={styles.skill}>
                    <View style={styles.skillHeader}>
                            <Text style={styles.skillHeaderValue}>Programming Language</Text>
                            <View style={{borderBottomColor:'black',borderBottomWidth: 1}}/>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon3 name="html5" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>HTML5</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level3.png')}/>
                        </View>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon3 name="css3" size={20}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>CSS</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level2.png')}/>
                        </View>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon4 name="js" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>Javascript</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level1.png')}/>
                        </View>
                    </View>                
                </View>
                <View style={styles.skill}>
                    <View style={styles.skillHeader}>
                            <Text style={styles.skillHeaderValue}>Framework</Text>
                            <View style={{borderBottomColor:'black',borderBottomWidth: 1}}/>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon5 name="bootstrap" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>Bootstrap</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level3.png')}/>
                        </View>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon5 name="react" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>React</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level1.png')}/>
                        </View>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon5 name="vuejs" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>Vue</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level2.png')}/>
                        </View>
                    </View>                
                </View>
                <View style={styles.skill}>
                    <View style={styles.skillHeader}>
                            <Text style={styles.skillHeaderValue}>Other</Text>
                            <View style={{borderBottomColor:'black',borderBottomWidth: 1}}/>
                    </View>
                    <View style={styles.skillList}>
                        <View style={styles.skillLogo}>
                            <Icon3 name="git" size={25} style={{marginBottom:5}}/>
                            <Icon3 name="github" size={25}/>
                        </View>
                        <View style={styles.skillDesc}>
                            <Text>Git & GitHub</Text>
                            <Image style={{width:240, resizeMode: 'contain'}} source={require('./assets/level2.png')}/>
                        </View>
                    </View>
                </View>
                <View style={{marginVertical:10}} />
            </ScrollView>
            <View style={styles.botNav}>
                <TouchableOpacity style={[styles.botNavList, styles.activeNav, styles.borderNav]}>
                    <Icon style={{color:'white', marginRight:5}} name="list" size={25}/>
                    <Text style={styles.botNavTitle}>Skill</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.botNavList}>
                    <Icon3 style={{color:'white', marginRight:5}} name="user" size={25}/>
                    <Text style={styles.botNavTitle}>About</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 30,
    },
    topNav: {  
        height: 40,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    topNavTitle: {
        fontSize: 14,
        marginRight: 5,
    },
    header: { 
        height: 183,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
    },
    headerImg: {
        height: 183,
        width: 300,
    },
    profile: {        
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    profileImg: {
        height: 60,
        width: 60,
        borderRadius: 180,
        marginRight: 20,
    },
    profileName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#666666',
    },
    skill: {        
        marginHorizontal: 20,
        marginBottom: 20,
    },
    skillHeader: {
        marginBottom: 10,
    },
    skillHeaderValue: {
        color: '#8a8a8a',
    },
    skillList: {
        marginVertical: 4,
        display: 'flex',
        flexDirection: 'row',
    },
    skillLogo: {
        marginHorizontal: 20,
    },
    botNav: { 
        height: 60,
        display: 'flex',
        flexDirection: 'row',
        alignSelf: "stretch",
        marginTop: 10,
    },
    botNavList: { 
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        backgroundColor: '#9c9c9c',
    },
    botNavTitle: {
        color: 'white',
        fontSize: 24,
    },
    activeNav: {
        backgroundColor: '#27A8E2',
    },
    borderNav: {
        borderRightColor: 'white',
        borderRightWidth: 3,
    },
});