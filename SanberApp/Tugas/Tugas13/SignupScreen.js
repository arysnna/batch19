import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Alert, ScrollView, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function App() {
    return (
        <ScrollView style={styles.container}>
            <View>
                <TouchableOpacity style={styles.topNav}>
                    <Icon name="arrow-back" size={25}/>
                    <Text style={styles.topNavTitle}>Signin</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                <Image source={require('./assets/logo.jpg')} style={styles.headerImg} />
            </View>
            <View style={styles.form}>
                <Text style={styles.formTitle}>Signup</Text>
                <TextInput style={styles.formInput} placeholder={'Email'}/>
                <TextInput style={styles.formInput} placeholder={'Password'} secureTextEntry={true} />
                <View style={styles.formError}>
                    <Text style={styles.formErrorValue}>*must be at least 6 characters</Text>
                </View>
                <TextInput style={styles.formInput} placeholder={'Confirm Password'} secureTextEntry={true} />
                <View style={styles.formError}>
                    <Text style={styles.formErrorValue}>*Your password and confirmation password do not match</Text>
                </View>
                <TouchableOpacity style={styles.formButton} >
                    <Text style={styles.formButtonTitle}> signup </Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        paddingTop: 30,
    },
    topNav: {  
        height: 40,
        flexDirection: 'row',
    },
    topNavTitle: {
        fontSize: 18,
    },
    header: { 
        height: 183,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
    },
    headerImg: {
        height: 183,
        width: 300,
    },
    form: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    formTitle: {
        color: '#919191',
        fontSize: 24,
        fontWeight: 'bold',
    },
    formError: {
        width: 200, 
    },
    formErrorValue: {
        color: 'red',
        fontSize: 12,
        fontWeight: '100',
        marginTop: 5,
    },
    formInput: {
        height: 40, 
        width: 200, 
        borderRadius: 8,
        marginTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#F5F5F5',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    formButton: {
        height: 40, 
        width: 200, 
        borderRadius: 8,
        marginTop: 20,
        paddingHorizontal: 10,
        backgroundColor: '#0085FF',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    formButtonTitle: {
        color: 'white',
        fontSize: 18,
    },
});