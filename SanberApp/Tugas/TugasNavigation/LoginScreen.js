import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

export const Login = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Text>Login Screen</Text>
      <Button title="Menuju Skill Screen" onPress={() => navigation.push('SkillScr')} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});