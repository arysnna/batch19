import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator} from "@react-navigation/drawer";

import { Login } from "./LoginScreen";
import { About } from "./AboutScreen";
import { Skill } from "./SkillScreen";
import { Project } from "./ProjectScreen";
import { Add } from "./AddScreen";

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const AuthStack = createStackNavigator();

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name='Skill' component={Skill} />
        <Tabs.Screen name='Project' component={Project} />
        <Tabs.Screen name='Add' component={Add} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='Home' component={TabsScreen} />
        <Drawer.Screen name='About' component={About} />
    </Drawer.Navigator>
)

export default() => (
    <NavigationContainer>
    <AuthStack.Navigator>
        <AuthStack.Screen name='Login' component={Login} options={{title: 'Sign In'}} />
        <AuthStack.Screen name='SkillScr' component={DrawerScreen} options={{title: 'Skill Screen'}} />
    </AuthStack.Navigator>
    </NavigationContainer>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});