import { StatusBar } from 'expo-status-bar';
import React from 'react';  
import { StyleSheet, Text, View } from 'react-native';
import Quiz from './Tugas/Quiz3/index';

export default function App() {
  return (
    <View style={styles.container}>
      <Quiz />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
