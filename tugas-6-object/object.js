// ==== nomor 1
console.log('');
console.log('// =============================== nomor 1');

function arrayToObject(arr) {
    let newArr = {};
    countAge = (birth) => {
        let age = '';
        if(birth != null) {
            const now = new Date().getFullYear(); 
            if(birth<=now) {
                age = now-birth;
            }else {
                age = 'invalid birth year';
            }
        }else {
            age = 'invalid birth year';
        }
        return age;
    }

    // const key = ['firstName', 'lastName', 'gender', 'age'];
    const arrlen = arr.length;
    for(let x=0; x<arrlen; x++) {
        // arr[x] = key.reduce((obj, key, index) => ({ ...obj, [key]: arr[x][index]}), {}); 
        setTitleData = (arr, x) => { 
            nomor = x+1;
            return nomor+'. '+arr[0]+' '+arr[1] 
        }
        setBioData = (arr) => {
            data = {
                firstName : arr[0],
                lastName : arr[1],
                gender : arr[2],
                age : countAge(arr[3])
            }
            return data;
        }  
        let bioData = setBioData(arr[x]);
        let setTitle = setTitleData(arr[x], x);
        newArr[setTitle] = bioData;
    }
    return newArr;
}
 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
console.log(arrayToObject(people));
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
console.log(arrayToObject(people2));
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
console.log(arrayToObject([])); // ""

// ==== nomor 2
console.log('');
console.log('// =============================== nomor 2');

function shoppingTime(memberId, money) {
    if(memberId=='' || memberId==null) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }else if(money<50000) {
        return 'Mohon maaf, uang tidak cukup';
    }else {
        let output = [];
        let listPurchased = [];
        let changeMoney = money;
        countList = (changeMoney) => {
            for(;changeMoney>=50000;) {
                if(changeMoney>=1500000) {
                    listPurchased.push('Sepatu Stacattu');
                    changeMoney-=1500000;
                }else if(changeMoney>=500000) {
                    listPurchased.push('Baju Zoro');
                    changeMoney-=500000;
                }else if(changeMoney>=250000) {
                    listPurchased.push('Baju H&N');
                    changeMoney-=250000;
                }else if(changeMoney>=175000) {
                    listPurchased.push('Sweater Uniklooh');
                    changeMoney-=175000;
                }else if(changeMoney>=50000) {
                    let check = listPurchased.indexOf('Casing Handphone');
                    if(check==-1) {
                        listPurchased.push('Casing Handphone');
                        changeMoney-=50000;
                    }else {break;}
                }
            }
            const x = {listPurchased, changeMoney}
            return x;
        }

        let count = countList(money);
        output = {
            'memberId': memberId,
            'money': money,
            'listPurchased': count['listPurchased'],
            'changeMoney': count['changeMoney']
        }

        return output;
    }
}
   
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// ==== nomor 3
console.log('');
console.log('// =============================== nomor 3');

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    output = [];
    for(i=0;i<arrPenumpang.length;i++){
        bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1]))*2000;
        transaksi = { 
            'penumpang': arrPenumpang[i][0],
            'naikDari': arrPenumpang[i][1],
            'tujuan': arrPenumpang[i][2],
            'bayar': bayar,
        }
        output.push(transaksi);
    }    
    return output;
}
 
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]